# Patchy Particles #

This code uses [mbuild](https://mosdef.org/mbuild/index.html), [hoomd](https://hoomd-blue.readthedocs.io/en/stable/), [signac](https://signac.io), 
and [cme_utils/convert_rigid](https://bitbucket.org/cmelab/cme_utils/src/master/cme_utils/manip/convert_rigid.py)

### What is this repository for? ###

This repository allows you to run your own simulation with the patchy-particles! 

![simulation in action](trajectory.gif)

### How do I get set up? ###

To install:

1. Create a conda environment and install the necessary packages:


        conda create -n patchy-particles python=3.7
        conda activate patchy-particles
        conda install -c conda-forge -c omnia -c mosdef numpy matplotlib py3dmol jupyter hoomd freud gsd signac signac-flow
        conda install -c mosdef -c omnia mbuild foyer --only-deps

2. Now clone and install the cme_utils package:


        git clone git@bitbucket.org:cmelab/cme_utils.git
        cd cme_utils
        pip install .

3. To run:

        jupyter notebook Self-Assembly_With_2D_Model.ipynb
    and run the cells in there!
