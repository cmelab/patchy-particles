#This will have all of the different trapezoids that we want to test with. 
# We will then import this file into project.py or any other file we want to run these in
import numpy as np
import mbuild as mb

class Capsid(mb.Compound):
    def __init__(self):
        super(Capsid, self).__init__()
        
        triangleheight = np.sqrt(3) * 0.5
        length = 6
        #trapezoidheight = 4 * triangleheight
        trap_particles = {}
        
        #grey - base building block
        lj_particle1 = mb.Particle(name='Z', pos=[5, -triangleheight*2, 0]) 
        trap_particles['zero-zero particle'] = lj_particle1
        self.add(lj_particle1)
        
        #grey - base building block
        lj_particle2 = mb.Particle(name='Z', pos=[4, -triangleheight*2, 0])
        trap_particles['zero-1 particle'] = lj_particle2
        self.add(lj_particle2)
        
        #grey - base building block
        lj_particle3 = mb.Particle(name='Z', pos=[4.5, -triangleheight, 0])
        trap_particles['-0.5-height particle'] = lj_particle3
        self.add(lj_particle3)
        
        ##### Make the bottom layer of the trapezoid #####
        row1x = length - 2
        row2x = length - 1.5
        row3x = length - 1
        row4x = length - 1.5
        row5x = length - 2
        counter = 0
        x = 9
        
        for particleid in range(x): 
            lj_particle = mb.Particle(name='Z', pos=[row1x - 1,-triangleheight*2,0])
            trap_particles['row1particle' + str(particleid)] = lj_particle
            self.add(lj_particle)
            lj_particle = mb.Particle(name='Z', pos=[row2x - 1, -triangleheight,0])
            trap_particles['row2particle' + str(particleid)] = lj_particle
            self.add(lj_particle)
            lj_particle = mb.Particle(name='Z', pos=[row3x - 1, 0,0])
            trap_particles['row3particle' + str(particleid)] = lj_particle
            self.add(lj_particle)
            if(counter < x - 1):
                lj_particle = mb.Particle(name='Z', pos=[row4x - 1, triangleheight,0])
                trap_particles['row4particle' + str(particleid)] = lj_particle
                self.add(lj_particle)
            if(counter < x - 2):
                lj_particle = mb.Particle(name='Z', pos=[row5x - 1, triangleheight * 2,0])
                trap_particles['row5particle' + str(particleid)] = lj_particle
                self.add(lj_particle)
            row1x = row1x - 1
            row2x = row2x - 1
            row3x = row3x - 1
            row4x = row4x - 1
            row5x = row5x - 1
            counter = counter + 1
        #print(trap_particles)


# GENERAL TRAPEZOID SO I KNOW WHERE THE PARTICLES ARE:
# [bottom_right, bottom4Rmid, right_b_mid, bottom3Rmid, 'Z', right_middle, right_u_mid, top_right, bottom2Rmid, 'Z', 'Z', 'Z', top2Rmid, bottom1Rmid, 'Z', 'Z', 'Z', top1Rmid, bottom_middle, 'Z', 'Z', 'Z', top_middle, bottom1Lmid, 'Z', 'Z', 'Z', top1Lmid, bottom2Lmid, 'Z',     'Z', 'Z', top2Lmid, bottom3Lmid, 'Z', 'Z', 'Z', top_left, bottom4Lmid, 'Z', 'Z', left_u_mid, bottom_left, left_b_mid, left_middle]

# Attaching the words to specific indices so we can specify them!
index_names = {}
for i,word  in enumerate(['bottom_right', 'bottom4Rmid', 'right_b_mid', 'bottom3Rmid', 'Z', 'right_middle', 'right_u_mid', 'top_right', 'bottom2Rmid', 'Z', 'Z', 'Z', 'top2Rmid', 'bottom1Rmid', 'Z', 'Z', 'Z', 'top1Rmid', 'bottom_middle', 'Z', 'Z', 'Z', 'top_middle', 'bottom1Lmid', 'Z', 'Z', 'Z', 'top1Lmid', 'bottom2Lmid', 'Z',     'Z', 'Z', 'top2Lmid', 'bottom3Lmid', 'Z', 'Z', 'Z', 'top_left', 'bottom4Lmid', 'Z', 'Z', 'left_u_mid', 'bottom_left', 'left_b_mid', 'left_middle']):
   # print(i,word)
    if word is not 'Z':
        index_names[word]=i
# print(index_names)

# Example
sample_trapezoid = ['Z']*45
sample_trapezoid[index_names['bottom3Lmid']]= 'A'
#print(sample_trapezoid)

# Creating dictionary for trap_types
trap_types={}

trap_types['eric_sample']= sample_trapezoid

# Sticky Particles on top_right, top2Rmid, bottom_left, left_b_mid
alpha = ['Z']*45
alpha[index_names['top_right']] = 'C'
alpha[index_names['top2Rmid']] = 'C'
alpha[index_names['bottom_left']] = 'C'
alpha[index_names['left_b_mid']] = 'C'
trap_types["Alpha"] = alpha

# Sticky Particles on top_right, top2Rmid, top1Rmid, bottom_left, left_b_mid, left_middle
beta = ['Z']*45
beta[index_names['top_right']] = 'C'
beta[index_names['top2Rmid']] = 'C'
beta[index_names['top1Rmid']] = 'C'
beta[index_names['bottom_left']] = 'C'
beta[index_names['left_b_mid']] = 'C'
beta[index_names['left_middle']] = 'C'
trap_types["Beta"] = beta

# Sticky Particles on top_right (A), top2Rmid (A), bottom_left (B), left_b_mid (B)
gamma = ['Z']*45
gamma[index_names['top_right']] = 'A'
gamma[index_names['top2Rmid']] = 'A'
gamma[index_names['bottom_left']] = 'B'
gamma[index_names['left_b_mid']] = 'B'
trap_types["Gamma"] = gamma

# Sticky Particles on top_right (A), top2Rmid (A), bottom_left (G), left_b_mid (G)
gamma1 = ['Z']*45
gamma1[index_names['top_right']] = 'A'
gamma1[index_names['top2Rmid']] = 'A'
gamma1[index_names['bottom_left']] = 'G'
gamma1[index_names['left_b_mid']] = 'G'
trap_types["Gamma1"] = gamma1

# Sticky Particles on top_right (D), top2Rmid (D), bottom_left (B), left_b_mid (B)
gamma2 = ['Z']*45
gamma2[index_names['top_right']] = 'D'
gamma2[index_names['top2Rmid']] = 'D'
gamma2[index_names['bottom_left']] = 'B'
gamma2[index_names['left_b_mid']] = 'B'
trap_types["Gamma2"] = gamma2

# Sticky Particles on top_right (F), top2Rmid (F), bottom_left (E), left_b_mid (E)
gamma3 = ['Z']*45
gamma3[index_names['top_right']] = 'F'
gamma3[index_names['top2Rmid']] = 'F'
gamma3[index_names['bottom_left']] = 'E'
gamma3[index_names['left_b_mid']] = 'E'
trap_types["Gamma3"] = gamma3

# Sticky Particles on top-middle (A), top1Rmid (A), left_b_mid (B), left_middle (B)
delta = ['Z']*45
delta[index_names['top_middle']] = 'A'
delta[index_names['top1Rmid']] = 'A'
delta[index_names['left_b_mid']] = 'B'
delta[index_names['left_middle']] = 'B'
trap_types["Delta"] = delta

# Sticky Particles on top-middle (A), top1Rmid (A), left_b_mid (G), left_middle (G)
delta1 = ['Z']*45
delta1[index_names['top_middle']] = 'A'
delta1[index_names['top1Rmid']] = 'A'
delta1[index_names['left_b_mid']] = 'G'
delta1[index_names['left_middle']] = 'G'
trap_types["Delta1"] = delta1

# Sticky Particles on top-middle (D), top1Rmid (D), left_b_mid (B), left_middle (B)
delta2 = ['Z']*45
delta2[index_names['top_middle']] = 'D'
delta2[index_names['top1Rmid']] = 'D'
delta2[index_names['left_b_mid']] = 'B'
delta2[index_names['left_middle']] = 'B'
trap_types["Delta2"] = delta2

# Sticky Particles on top-middle (F), top1Rmid (F), left_b_mid (E), left_middle (E)
delta3 = ['Z']*45
delta3[index_names['top_middle']] = 'F'
delta3[index_names['top1Rmid']] = 'F'
delta3[index_names['left_b_mid']] = 'E'
delta3[index_names['left_middle']] = 'E'
trap_types["Delta3"] = delta3

# Sticky Particles on top1Rmid (A), top2Rmid (A), left_b_mid (B), left_middle (B)
zeta = ['Z']*45
zeta[index_names['top1Rmid']] = 'A'
zeta[index_names['top2Rmid']] = 'A'
zeta[index_names['left_b_mid']] = 'B'
zeta[index_names['left_middle']] = 'B'
trap_types["Zeta"] = zeta

# Sticky Particles on top1Rmid (A), top2Rmid (A), left_b_mid (G), left_middle (G)
zeta1 = ['Z']*45
zeta1[index_names['top1Rmid']] = 'A'
zeta1[index_names['top2Rmid']] = 'A'
zeta1[index_names['left_b_mid']] = 'G'
zeta1[index_names['left_middle']] = 'G'
trap_types["Zeta1"] = zeta1

# Sticky Particles on top1Rmid (D), top2Rmid (D), left_b_mid (B), left_middle (B)
zeta2 = ['Z']*45
zeta2[index_names['top1Rmid']] = 'D'
zeta2[index_names['top2Rmid']] = 'D'
zeta2[index_names['left_b_mid']] = 'B'
zeta2[index_names['left_middle']] = 'B'
trap_types["Zeta2"] = zeta2

# Sticky Particles on top1Rmid (F), top2Rmid (F), left_b_mid (E), left_middle (E)
zeta3 = ['Z']*45
zeta3[index_names['top1Rmid']] = 'F'
zeta3[index_names['top2Rmid']] = 'F'
zeta3[index_names['left_b_mid']] = 'E'
zeta3[index_names['left_middle']] = 'E'
trap_types["Zeta3"] = zeta3

# Sticky Particles on top-middle, top1Rmid, left_b_mid, left_middle
eta = ['Z']*45
eta[index_names['top_middle']] = 'C'
eta[index_names['top1Rmid']] = 'C'
eta[index_names['left_b_mid']] = 'C'
eta[index_names['left_middle']] = 'C'
trap_types["Eta"] = eta

# Sticky Particles on top1Rmid, top2Rmid, left_b_mid, left_middle
iota = ['Z']*45
iota[index_names['top1Rmid']] = 'C'
iota[index_names['top2Rmid']] = 'C'
iota[index_names['left_b_mid']] = 'C'
iota[index_names['left_middle']] = 'C'
trap_types["Iota"] = iota

# Sticky Particles on top2Rmid (A), top_right (B), bottom_left (A), left_b_mid (B)
kappa = ['Z']*45
kappa[index_names['top_right']] = 'B'
kappa[index_names['top2Rmid']] = 'A'
kappa[index_names['bottom_left']] = 'A'
kappa[index_names['left_b_mid']] = 'B'
trap_types["Kappa"] = kappa

# Sticky Particles on top2Rmid (C), top_right (D), bottom_left (E), left_b_mid (F)
trap_types["Kappa1"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'D', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'E', 'F', 'Z']
# Sticky Particles on top2Rmid (L), top_right (K), bottom_left (B), left_b_mid (A)
trap_types["Kappa2"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'K', 'Z', 'Z', 'Z', 'Z', 'L', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'B', 'A', 'Z']
# Sticky Particles on top2Rmid (G), top_right (H), bottom_left (I), left_b_mid (J)
trap_types["Kappa3"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'H', 'Z', 'Z', 'Z', 'Z', 'G', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'I', 'J', 'Z']

# Sticky Particles on top-middle (B), top1Rmid (A), left_b_mid (A), left_middle (B)
lamda = ['Z']*45
lamda[index_names['top_middle']] = 'B'
lamda[index_names['top1Rmid']] = 'A'
lamda[index_names['left_middle']] = 'B'
lamda[index_names['left_b_mid']] = 'A'
trap_types["Lamda"] = lamda

# Sticky Particles on top-middle (C), top1Rmid (D), left_b_mid (E), left_middle (F)
trap_types["Lamda1"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'D', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'E', 'F']
# Sticky Particles on top-middle (L), top1Rmid (K), left_b_mid (B), left_middle (A)
trap_types["Lamda2"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'K', 'Z', 'Z', 'Z', 'Z', 'L', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'B', 'A']
# Sticky Particles on top-middle (G), top1Rmid (H), left_b_mid (I), left_middle (J)
trap_types["Lamda3"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'H', 'Z', 'Z', 'Z', 'Z', 'G', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'I', 'J']

# Sticky Particles on top1Rmid (B), top2Rmid (A), left_b_mid (A), left_middle (B)
omicron = ['Z']*45
omicron[index_names['top1Rmid']] = 'B'
omicron[index_names['top2Rmid']] = 'A'
omicron[index_names['left_middle']] = 'B'
omicron[index_names['left_b_mid']] = 'A'
trap_types["Omicron"] = omicron

# Sticky Particles on top1Rmid (C), top2Rmid (D), left_b_mid (E), left_middle (F)
trap_types["Omicron1"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'D', 'Z', 'Z', 'Z', 'Z', 'C', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'E', 'F']
# Sticky Particles on top1Rmid (G), top2Rmid (H), left_b_mid (I), left_middle (J)
trap_types["Omicron3"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'H', 'Z', 'Z', 'Z', 'Z', 'G', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'I', 'J']
# Sticky Particles on top1Rmid (L), top2Rmid (K), left_b_mid (B), left_middle (A)
trap_types["Omicron2"] = ['Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'K', 'Z', 'Z', 'Z', 'Z', 'L', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'Z', 'B', 'A']
