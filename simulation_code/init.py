import signac


def main():
    project = signac.init_project("Patchy-Particles")
    for kT in [0.5, 1, 2, 5]:
        for trap_type in [
            "Delta",
            "Kappa",
        ]:
            statepoint = {"kT": kT}
            statepoint["sticky"] = True
            statepoint["epsilon"] = 50
            statepoint["mix_kT"] = 50
            statepoint["trap_type"] = trap_type
            job = project.open_job(statepoint)
            job.init()


if __name__ == "__main__":
    main()
